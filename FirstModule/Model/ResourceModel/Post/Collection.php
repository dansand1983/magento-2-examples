<?php

namespace Examples\FirstModule\Model\ResourceModel\Post;

use Examples\FirstModule\Model\Post as PostModel;
use Examples\FirstModule\Model\ResourceModel\Post as PostResourceModel;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    /**
     * Define model / resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(PostModel::class, PostResourceModel::class);
    }
}
