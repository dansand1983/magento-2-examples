<?php

namespace Examples\FirstModule\Model;

use Examples\FirstModule\Api\Data\PostInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractExtensibleModel;

class Post extends AbstractExtensibleModel implements PostInterface, IdentityInterface
{
    const CACHE_TAG = 'examples_first_module_post';

    protected function _construct()
    {
        $this->_init(ResourceModel\Post::class);
        $this->setIdFieldName('post_id');
    }

    public function getIdentities(): array
    {
        return [self::CACHE_TAG . '_' . $this->getPostId()];
    }

    public function getPostId(): int
    {
        return $this->getData(self::POST_ID);
    }

    public function getAuthorName(): string
    {
        return $this->getData(self::AUTHOR_NAME);
    }

    public function getEmail(): string
    {
        return $this->getData(self::EMAIL);
    }

    public function getContent(): string
    {
        return $this->getData(self::CONTENT);
    }

    public function getCreatedAt(): string
    {
        return $this->getData(self::CREATED);
    }

    public function getUpdatedAt(): string
    {
        return $this->getData(self::UPDATED);
    }

    public function setPostId(int $postId): PostInterface
    {
        return $this->setData(self::POST_ID, $postId);
    }

    public function setAuthorName(string $authorName): PostInterface
    {
        return $this->setData(self::AUTHOR_NAME, $authorName);
    }

    public function setEmail(string $email): PostInterface
    {
        return $this->setData(self::EMAIL, $email);
    }

    public function setContent(string $content): PostInterface
    {
        return $this->setData(self::CONTENT, $content);
    }

    public function setCreatedAt(string $createdAt): PostInterface
    {
        return $this->setData(self::CREATED, $createdAt);
    }

    public function setUpdatedAt(string $updatedAt): PostInterface
    {
        return $this->setData(self::UPDATED, $updatedAt);
    }

    /**
     * Prepare data before saving
     *
     * @return PostInterface
     */
    public function beforeSave(): PostInterface
    {
        if ($this->hasDataChanges()) {
            $this->setUpdatedAt(date("Y-m-d H:i:s"));
        }
        return parent::beforeSave();
    }
}
