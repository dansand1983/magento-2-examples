<?php

namespace Examples\FirstModule\Block\Adminhtml\Examples;

use Examples\FirstModule\Api\Data\PostInterface;
use Magento\Backend\Block\Template\Context;
use Magento\Directory\Helper\Data as DirectoryHelper;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Json\Helper\Data as JsonHelper;

class SimplePosts extends \Magento\Backend\Block\Template
{


    public function __construct(
        Context $context,
        \Examples\FirstModule\Api\PostRepositoryInterface $postRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder                             $searchCriteriaBuilder,
        \Magento\Framework\Api\FilterBuilder              $filterBuilder,
        array $data = [],
        ?JsonHelper $jsonHelper = null,
        ?DirectoryHelper $directoryHelper = null
    ) {
        parent::__construct($context, $data, $jsonHelper, $directoryHelper);
        $this->postRepository = $postRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
    }

    /**
     * @return PostInterface[]
     * @throws LocalizedException
     */
    public function getPosts(): array
    {
        $filter = $this->filterBuilder->setField(PostInterface::POST_ID)
            ->setConditionType('gt')
            ->setValue(0)
            ->create();
        $searchCriteria = $this->searchCriteriaBuilder->addFilters([$filter])->create();
        return $this->postRepository->getList($searchCriteria)->getItems();
    }
}
