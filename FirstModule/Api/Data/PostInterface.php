<?php

namespace Examples\FirstModule\Api\Data;

interface PostInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const POST_ID = 'post_id';
    const AUTHOR_NAME = 'author_name';
    const EMAIL = 'email';
    const CONTENT = 'content';
    const CREATED = 'created';
    const UPDATED = 'updated';

    /**
     * @return int
     */
    public function getPostId(): int;

    /**
     * @return string
     */
    public function getAuthorName(): string;

    /**
     * @return string
     */
    public function getEmail(): string;

    /**
     * @return string
     */
    public function getContent(): string;

    /**
     * Get created at
     *
     * @return string
     */
    public function getCreatedAt(): string;

    /**
     * Get created at
     *
     * @return string
     */
    public function getUpdatedAt(): string;

    /**
     * @param int $postId
     * @return PostInterface
     */
    public function setPostId(int $postId): PostInterface;

    /**
     * @param string $authorName
     * @return PostInterface
     */
    public function setAuthorName(string $authorName): PostInterface;

    /**
     * @param string $email
     * @return PostInterface
     */
    public function setEmail(string $email): PostInterface;

    /**
     * @param string $content
     * @return PostInterface
     */
    public function setContent(string $content): PostInterface;

    /**
     * Get created at
     *
     * @param string $createdAt
     * @return PostInterface
     */
    public function setCreatedAt(string $createdAt): PostInterface;

    /**
     * Get created at
     *
     * @param string $updatedAt
     * @return PostInterface
     */
    public function setUpdatedAt(string $updatedAt): PostInterface;
}
